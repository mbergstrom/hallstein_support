# Adjusting your subscription

All of your active subscriptions can be seen at the top of your account page, and you have the option to adjust the quantity and frequency of any deliveries that are not already in progress.

You also have the option to “Delay Delivery” of your next shipment should you need to do so. 