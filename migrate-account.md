# Migrate Account

As a result of significant updates to our online shop our current customers existing login details will no longer work. Don’t worry, we have done all we can to make this transition as painless as possible.
On the landing screen, click the bottom option titled “Have an Old Account?”, and then follow the steps.

[Start Migration Process](#migration)

## Step 1

You will be asked to enter your existing Username and Password. 

Once filled in, click “Ok” to move on to the next step. The system will then check to see if your account exists.

## Step 2

Once your account has been verified you will be asked to fill in new account details, including Name, Email, Telephone (optional) and a new Password.

Tick the check box to agree to our Privacy Policy and Terms & Conditions once you have read and accept them. Here you also have the opportunity to opt in to receive our newsletter. 

Click “OK” when you are happy to proceed.

## Step 3

All done! You can now click the “Sign In” button to continue to your account.

## Please note 

You need to supply your old login details to be able to migrate your account to this new system. Those will be the same details that you have used before to access your subscription.

If you have any problems, please don't hesitate to contact us.

[Contact Support](#support:contact)