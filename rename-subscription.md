# Rename your subscription

If you have more than one subscription you may wish to rename them in order to easily identify one from the other. 

To do this, click on the “Rename subscription” button, and input the new name.

Clicking the “Ok” button will update it.