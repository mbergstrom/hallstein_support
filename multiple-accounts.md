# Creating Multiple Accounts

One account can have multiple subscriptions, so there is no need to setup another for this purpose. Reasons that you might want to setup another account could be: 

- To give another person access to the account
- To hand over the account to somebody else
- To have a different billing address
- To have an account in another country (coming soon)

## Step 1

New accounts can be created from within your existing account by clicking the link in the top right hand corner of the menu where it displays your account name and “Switch or add account”. 

In the popup that follows, click on the button labelled “Add account”.

## Step 2

Fill in the name you would like to give to your new account, and choose the shipping destination from the drop down menu.

Click “Ok” to create the account.
