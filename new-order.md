# Making an order

Once you have an active subscription you will be able to make one off orders for additional products from Hallstein. 

If you require extra bottles of water for example, the option to order them apart from your subscription quota will now be available when visiting the shop.

## Get started

The process for an Order is the same as for a subscription. 

Begin by adding an item to the cart, and then click “View Cart”. Once you’re happy, proceed by clicking the “Checkout” button, and completing the Shipping and Billing details.

Having reviewed your order, click the “Place Order” button to complete the process.

Unlike a subscription, Orders are processed immediately. You can view the status of an order in the “Deliveries” section of your account page.

## Note

Because orders are processed immediately, they are not adjustable. If you wish to cancel an order, please contact us as soon as possible. 

[Contact Support](#support:contact)