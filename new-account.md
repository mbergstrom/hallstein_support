# Create a new account 1

Creating a new account is a simple, three step process:

## Step 1

Select the country you would like us to deliver your water. If your country is not listed, you can join the waiting list and we will let you know when it becomes available. 

Click “Continue” to proceed.

## Step 2

Fill in the contact details for your account, including Name, Email, Password and Telephone (optional). 

Tick the check box to agree to our Privacy Policy and Terms & Conditions once you have read and accept them. Here you also have the opportunity to opt in to receive our newsletter. 

Click “OK” when you are happy to proceed.

## Step 3

Once you are registered with our system, you are ready to go.

Click the “Sign In” button to access your account.
