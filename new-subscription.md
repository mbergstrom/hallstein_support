# Create a new subscription

Your subscription to Hallstein is at the heart of your account. To begin setting it up, follow the main button titled “Go to shop”.

## Step 1

Once in the shop you will see the option to add a Hallstein Water Subscription to your account. Click the “Add to cart” button. 

Tip: It is not possible to purchase any other products in the shop without a subscription. Please add this to your cart first.

## Step 2

In the pop up window you can select how many bottles you would like to receive per delivery, and the frequency you will receive those deliveries. 

If you are unsure how many bottles you will need, the “Quantity Assistant” button can be used to calculate for you. This will then auto populate the quantity and frequency fields.

Click “Ok” once you are happy with your choice.

## Step 3

Now that your subscription is in your cart, you have the option to add a dispenser. If you do not already have one, click the “Add to cart” button on your preference.

At the bottom of your browser you will see an option to “View Cart”. Click here to continue for an overview of your selected items.

## Step 4

When viewing your Shopping Cart you have the option to make any final adjustments depending on your selection. You can also “Continue Shopping” or “Clear Cart” from this screen.

To proceed to payment, shipping and billing details, click “Checkout”.

## Step 5

The final screen handles your Shipping Address, Billing Details and Billing Address.

After adding your Shipping Address the system will calculate your taxes, updating the Order Total in the Order Summary. 

Our available stock in your location is also checked at this stage.

## Step 6

Having completed your Shipping and Billing details, click the “Place Order” button.

Once the system has processed your order, you will receive a pop up notification with your order reference number. Clicking “Continue” will  take you back to your account overview.

## Please Note

If you receive an insufficient stock notice when adding your Shipping Address, it is most likely that it is the dispenser that is unavailable. 

If you are happy to only receive your water, you can try removing the dispenser and going through the checkout process again. 

In the unlikely event that we are unable to supply water to your State, please [Contact Support](#support:contact) for updates on availability.
