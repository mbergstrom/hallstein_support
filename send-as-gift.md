# Send as gift

From the shop, select the products you would like to gift and add them to your cart. When finished, go to checkout. 

## Checkout

Start by adding the shipping address of the person you wish to receive the gift. This allows the system to ensure we can deliver to their address.

Next, click the ‘send as a gift’ option below the shipping address. 

Add in a personal message to the recipient. Don’t forget to include who the gift is from in your message.

This message will be sent to the recipients email you specified when entering the shipping address. The same email will include courier tracking information and details of what is being delivered. The recipient will not be able to see the cost of the gift.

You will also receive an email notifying you when your gift has been sent.

Complete your billing details if necessary, and review the billing address.

You can now complete your order.