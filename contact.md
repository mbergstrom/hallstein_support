# Contact Support

## North America

For Customers in United States please contact:

**Alpine Water USA LLC**

70 Park Dr N, Rye, NY 10580
Telephone: <a href="tel:+18889889148">+1 (888) 988-9148</a>
[contact@hallsteinwater.com](mailto:contact@hallsteinwater.com)

---

## Canada

For customers in Canada please contact:

**Hallstein Water Canada Inc.**

6 Inwood Ave, Toronto, ON, M4J 3Y3, Canada
[canada@hallsteinwater.com](mailto:canada@hallsteinwater.com)

---

## Switzerland

For customers in Switzerland please contact:

**Alpine Water Schweiz GmbH**

c/o M Advice Maurerstrasse 2 8500 Frauenfeld
[switzerland@hallsteinwater.com](mailto:switzerland@hallsteinwater.com)

---

## Europe and Rest of the World

For customers in Europe and the rest of the world, please contact:

**Alpine Water GmbH**

HQ: Ried Ecking 12A, 6370 Kitzbühel
Production: Obertraun 311, 4831 Obertraun
Telephone: <a href="tel:+43720880885">+43 720 880 885</a>
UID Nr.: ATU61823414
[contact@hallsteinwater.com](mailto:contact@hallsteinwater.com)